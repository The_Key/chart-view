//
//  AppDelegate.h
//  ChartViewTest
//
//  Created by Kirill on 11/25/13.
//  Copyright (c) 2013 Kirill. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
