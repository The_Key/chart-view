//
//  ViewController.m
//  ChartViewTest
//
//  Created by Kirill on 11/25/13.
//  Copyright (c) 2013 Kirill. All rights reserved.
//

#import "ViewController.h"
#import "ChartView.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet ChartView *chart;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    self.chart.stepWidth = 40.0;
    self.chart.stepHeight = 20.0;

    self.chart.chartType = ChartTypeBar1;
    self.chart.firstColumnData = @[@(0.5),@(0.11),@(0.3),@(0.07)];
    self.chart.secondDataColumn = @[@(0.77),@(0.25),@(0.3741),@(0.64)];
    
//    [self.chart drawChartWithType:ChartTypeBar
//                  withFirstPoints:@[@(44),@(3),@(10),@(31)]
//                 withSecondPoints:@[@(45),@(3),@(24),@(31)]];

//    self.chart.chartType = ChartTypeLine2;
//    NSValue *v1 = [NSValue valueWithCGPoint:CGPointMake(44, 25)];
//    NSValue *v2 = [NSValue valueWithCGPoint:CGPointMake(78, 50)];
//    
//    NSValue *v3 = [NSValue valueWithCGPoint:CGPointMake(227, 100)];
//    NSValue *v4 = [NSValue valueWithCGPoint:CGPointMake(47, 90)];
//    
//    
//    self.chart.firstColumnData = @[v1,v2,v3];
//    self.chart.secondDataColumn = @[v3,v4,v2];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
