//
//  ChartView.h
//  ChartViewTest
//
//  Created by Kirill on 11/25/13.
//  Copyright (c) 2013 Kirill. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    ChartTypeLine = 10,
    ChartTypeBar1,
    ChartTypeBar2
} ChartType ;

@interface ChartView : UIView

@property (nonatomic) ChartType chartType;

@property (nonatomic) float stepWidth;
@property (nonatomic) float stepHeight;

@property (nonatomic, strong) NSArray *firstColumnData;
@property (nonatomic, strong) NSArray *secondDataColumn;

@property (nonatomic) NSRange horizontalAxisRange;
@property (nonatomic) NSRange verticalAxisRange;

@property (nonatomic) CGFloat horizontalStep;
@property (nonatomic) CGFloat verticalStep;


@end
