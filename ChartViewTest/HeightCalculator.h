//
//  HeightCalculator.h
//  ChartViewTest
//
//  Created by Kirill on 11/25/13.
//  Copyright (c) 2013 Kirill. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HeightCalculator : NSObject
+ (CGFloat)calculateHeightWithPercent:(CGFloat)percent
                     inAxisWithHeight:(CGFloat)axisHeight;
@end
