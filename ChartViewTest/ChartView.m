//
//  ChartView.m
//  ChartViewTest
//
//  Created by Kirill on 11/25/13.
//  Copyright (c) 2013 Kirill. All rights reserved.
//

#import "ChartView.h"
#import "HeightCalculator.h"

#define HORIZONTAL_AXIS_LEFT_PADDING 10

#define BAR_WIDTH_TYPE_1 30.0
#define BAR_WIDTH_TYPE_2 15.0

#define FIRST_BAR 0
#define SECOND_BAR 1

#define FIRST_BAR_COLOR     [UIColor greenColor].CGColor
#define SECOND_BAR_COLOR    [UIColor colorWithRed:196.0/255.0 green:196.0/255.0 blue:43.0/255.0 alpha:1.0].CGColor

#define POINT_RADIUS 4

#define WIDTH_OFFSET 2

@interface ChartView () {
    CGPoint zeroPoint;
}

@end

@implementation ChartView

- (void)awakeFromNib {
    [super awakeFromNib];
    zeroPoint = CGPointMake(0,
                            CGRectGetHeight(self.frame));
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
    }
    return self;
}

- (void)setStepHeight:(float)stepHeight {
            // when change drawing parameters - redraw image
    _stepHeight = stepHeight;
    [self setNeedsDisplay];
}

- (void)setStepWidth:(float)stepWidth {
    _stepWidth = stepWidth;
    [self setNeedsDisplay];
}

- (void)setFirstTypePoints:(NSArray *)firstTypePoints {
    _firstColumnData = firstTypePoints.copy;
    [self setNeedsDisplay];
}

- (void)setSecondTypePoints:(NSArray *)secondTypePoints {
    _secondDataColumn = secondTypePoints.copy;
    [self setNeedsDisplay];
}

- (void)drawCoordinateAxesWithContext:(CGContextRef)context {
    CGContextSetLineWidth(context, 1);
    CGContextSetAlpha(context, 1);

    CGContextSetStrokeColorWithColor(context, [UIColor blackColor].CGColor);
    
    // draw Y axe
    CGContextMoveToPoint(context, zeroPoint.x,zeroPoint.y);
    CGContextAddLineToPoint(context, zeroPoint.x, 0);
    CGContextDrawPath(context, kCGPathStroke);
    
    //draw X axe
    CGContextMoveToPoint(context, zeroPoint.x - HORIZONTAL_AXIS_LEFT_PADDING,
                                    CGRectGetHeight(self.frame));
    
    CGContextAddLineToPoint(context, CGRectGetMaxX(self.frame),
                                    CGRectGetHeight(self.frame));
    CGContextDrawPath(context, kCGPathStroke);
}

- (void)drawGridWithContext:(CGContextRef)context {
    // draw vertical axis
    CGContextSetLineWidth(context, 1);
    CGContextSetStrokeColorWithColor(context, [UIColor lightGrayColor].CGColor);
    CGContextSetAlpha(context, 0.2);
    
    float currentLineCoordinate = zeroPoint.x + self.stepWidth/2;
    
    while (currentLineCoordinate < CGRectGetWidth(self.frame) && self.stepWidth > 0) {
        CGContextMoveToPoint(context, currentLineCoordinate,0);
        // draw Y axe
        CGContextAddLineToPoint(context, currentLineCoordinate, CGRectGetHeight(self.frame));
        CGContextDrawPath(context,kCGPathStroke);
        
        currentLineCoordinate += self.stepWidth;
    }
    
    currentLineCoordinate = zeroPoint.y - self.stepHeight;

    while (currentLineCoordinate > zeroPoint.x && self.stepHeight > 0) {
        CGContextBeginPath(context);
        
        CGContextMoveToPoint(context, zeroPoint.x,currentLineCoordinate);

        CGContextAddLineToPoint(context,CGRectGetMaxX(self.frame), currentLineCoordinate);
        CGContextDrawPath(context,kCGPathStroke);
        
        currentLineCoordinate -= self.stepHeight;
    }
}

- (void)drawRect:(CGRect)rect {
    CGContextRef currentContext = UIGraphicsGetCurrentContext();
    
    switch (self.chartType) {
        case ChartTypeBar1: {
            [self drawBarChartFromPointsArray:self.firstColumnData
                         withStartXCoordinate:self.stepWidth/2 - BAR_WIDTH_TYPE_1/2
                                 withBarWidth:BAR_WIDTH_TYPE_1
                                 withBarColor:FIRST_BAR_COLOR
                                  withContext:currentContext];
        }
            break;
        case ChartTypeBar2: {
            NSAssert(self.firstColumnData.count == self.secondDataColumn.count, @"Number of data in first column should be equal to number of data in second column");
            
            [self drawBarChartFromPointsArray:self.firstColumnData
                         withStartXCoordinate:zeroPoint.x + BAR_WIDTH_TYPE_2/2 - WIDTH_OFFSET
                                 withBarWidth:BAR_WIDTH_TYPE_2
                                 withBarColor:FIRST_BAR_COLOR
                                  withContext:currentContext];
            
            [self drawBarChartFromPointsArray:self.secondDataColumn
                         withStartXCoordinate:BAR_WIDTH_TYPE_2/2 + BAR_WIDTH_TYPE_2 - WIDTH_OFFSET
                                 withBarWidth:BAR_WIDTH_TYPE_2
                                 withBarColor:SECOND_BAR_COLOR
                                  withContext:currentContext];
        }
            break;
            
        case ChartTypeLine: {
            [self drawLineChartWithPointsArray:self.firstColumnData
                                     withColor:FIRST_BAR_COLOR
                                   withContext:currentContext];
            
            if (self.secondDataColumn) {
                NSAssert(self.firstColumnData.count == self.secondDataColumn.count, @"Number of data in first column should be equal to number of data in second column");
                
                [self drawLineChartWithPointsArray:self.secondDataColumn
                                         withColor:SECOND_BAR_COLOR
                                       withContext:currentContext];
            }
            
        }
            break;
            
        default:
            break;
    }
    [self drawGridWithContext:currentContext];
    [self drawCoordinateAxesWithContext:currentContext];
}


- (CGFloat)calculatePointValue:(CGFloat)value {
    return CGRectGetHeight(self.frame) * value;
}


- (void)drawLineChartWithPointsArray:(NSArray *)points
                           withColor:(CGColorRef)color
                         withContext:(CGContextRef)context {
    CGFloat xCoordinate = self.stepWidth;
    for (int i = 0; i < points.count-1; i++) {
        NSNumber *firstValue = points[i];
        NSNumber *secondValue = points[i + 1];
        
        
        CGPoint firstPoint = CGPointMake(xCoordinate, [self calculatePointValue:firstValue.floatValue]);
        CGPoint secondPoint = CGPointMake(xCoordinate + self.stepWidth,[self calculatePointValue:secondValue.floatValue]);
        
        [self drawLineWithFirstPoint:firstPoint
                     withSecondPoint:secondPoint
                           withColor:color
                         withContext:context];
        
        xCoordinate += self.stepWidth;
    }
}

- (void)drawBarChartFromPointsArray:(NSArray *)points
               withStartXCoordinate:(CGFloat)startXCoordinate
                       withBarWidth:(CGFloat)barWidth
                       withBarColor:(CGColorRef)color
                        withContext:(CGContextRef)context {
    for (int i = 0; i < points.count; i++) {
        NSNumber *point = points[i];
        CGFloat barHeight = [self calculatePointValue:point.floatValue];
        CGRect barRect = CGRectMake(startXCoordinate, zeroPoint.y - barHeight, barWidth, barHeight);
        [self drawBarRect:barRect withColor:color withContext:context];
        startXCoordinate += self.stepWidth;
    }
}

- (void)drawLineWithFirstPoint:(CGPoint)firstPoint
               withSecondPoint:(CGPoint)secondPoint
                     withColor:(CGColorRef)color
                   withContext:(CGContextRef)context {
    CGContextSetFillColorWithColor(context, color);
    CGContextSetStrokeColorWithColor(context, color);
    CGContextMoveToPoint(context, firstPoint.x,firstPoint.y);
    
    CGContextAddLineToPoint(context, secondPoint.x, secondPoint.y);
    CGContextDrawPath(context, kCGPathStroke);
    CGContextFillEllipseInRect(context, CGRectMake(firstPoint.x - POINT_RADIUS/2,
                                                   firstPoint.y - POINT_RADIUS/2,
                                                   POINT_RADIUS,
                                                   POINT_RADIUS));
    
    CGContextFillEllipseInRect(context, CGRectMake(secondPoint.x - POINT_RADIUS/2,
                                                   secondPoint.y - POINT_RADIUS/2,
                                                   POINT_RADIUS,
                                                   POINT_RADIUS));
}

- (void)drawBarRect:(CGRect)barRect withColor:(CGColorRef)color withContext:(CGContextRef)context {
    CGContextSetFillColorWithColor(context, color);
    CGContextFillRect(context, barRect);
}

@end
